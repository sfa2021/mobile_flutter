import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (c) => MultiState(),
          )
        ],
        child: MaterialApp(
          title: 'PT SISI',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: MyHomePage(),
          debugShowCheckedModeBanner: false,
        ));
  }
}

class MultiState extends ChangeNotifier {
  String? token;

  setToken(String value) {
    this.token = value;
    notifyListeners();
  }
}

class MyHomePage extends StatelessWidget {
  MultiState? ms;

  @override
  Widget build(BuildContext context) {
    ms = Provider.of<MultiState>(context);
    return Scaffold(
        appBar: AppBar(
          title: Text('SISI Flutter Academy'),
        ),
        body: init(context));
  }

  init(BuildContext context){
    return Align(
      alignment: Alignment.topCenter,
      child: ElevatedButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (route) => ms!.token != null ? Profil() : Login(),
            ),
          );
        },
        child: Text(ms!.token != null ? 'Akun' : 'Masuk'),
      ),
    );
  }
}

class Login extends StatelessWidget {
  MultiState? ms;

  @override
  Widget build(BuildContext context) {
    ms = Provider.of<MultiState>(context);
    return ChangeNotifierProvider(
      create: (_) => KontenState(this.ms, context),
      child: Consumer<KontenState>(builder: (context, state, _) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Login"),
          ),
          body: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: state.uname,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Field ini wajib diisi';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: state.pass,
                    obscureText: true,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Field ini wajib diisi';
                      }
                      return null;
                    },
                  ),
                ),
                if (state.warning != null) Text("${state.warning}"),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: ElevatedButton(
                    onPressed: () {
                      state.prosesLogin();
                    },
                    child: const Text('Masuk'),
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}

class KontenState extends ChangeNotifier {
  MultiState? ms;
  TextEditingController uname = new TextEditingController();
  TextEditingController pass = new TextEditingController();
  List<String> data = [];
  String? warning;
  BuildContext context;

  KontenState(this.ms, this.context) {
    if (this.ms!.token != null) {
      getData();
    }
    print("$data");
  }

  getData() {
    if (data.length > 0) data.clear();
    this.data.add("Alex");
    this.data.add("alex@gmail.com");
    notifyListeners();
  }

  getDetailData() {
    print(this.warning);
    if (data.length > 0) data.clear();
    this.data.add("Alex");
    this.data.add("alex@gmail.com");
    this.data.add("27");
    this.data.add("Jl. Panglima Sudirman");
    this.data.add("Pria");
    notifyListeners();
  }

  prosesLogin() {
    if (uname.text != "alex") {
      this.warning = "Username salah";
    } else if (pass.text != "123") {
      this.warning = "Password salah";
    } else {
      ms!.setToken("123456789");
      Navigator.pop(context);
    }
    notifyListeners();
  }
}

class Profil extends StatelessWidget {
  MultiState? ms;

  @override
  Widget build(BuildContext context) {
    ms = Provider.of<MultiState>(context);
    return ChangeNotifierProvider(
      create: (_) => KontenState(this.ms, context),
      child: Consumer<KontenState>(builder: (context, state, _) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Profil"),
          ),
          body: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("${state.data[0]}"),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("${state.data[1]}"),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: ElevatedButton(
                    onPressed: () {
                      state.getDetailData();
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (route) => DetailProfil(state),
                        ),
                      );
                    },
                    child: const Text('Detai Profil'),
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}

class DetailProfil extends StatelessWidget {
  KontenState? state;

  DetailProfil(this.state);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: state,
      child: Consumer<KontenState>(builder: (context, state, _) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Detail Profil"),
          ),
          body: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("${state.data[0]}"),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("${state.data[1]}"),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("${state.data[2]}"),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("${state.data[3]}"),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("${state.data[4]}"),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}