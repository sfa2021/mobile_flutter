import 'package:flutter/material.dart';

class ContohSwitchTile extends StatefulWidget {
  const ContohSwitchTile({Key? key}) : super(key: key);

  @override
  _ContohSwitchTileState createState() => _ContohSwitchTileState();
}

class _ContohSwitchTileState extends State<ContohSwitchTile> {
  bool? _switchValue = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Contoh SwitchTile"),
        ),
        body: ListView(
          children: [
            Center(
              child: SwitchListTile(
                  title: Text("Cahaya"),
                  secondary: _switchValue == true
                      ? Icon(Icons.lightbulb, color: Colors.amberAccent)
                      : Icon(Icons.lightbulb_outline),
                  value: _switchValue!,
                  onChanged: (bool value) {
                    setState(() {
                      _switchValue = value;
                    });
                  }),
            ),
            Center(
              child: SwitchListTile(
                title: Text("On - Off"),
                secondary: Icon(Icons.lightbulb_outline),
                onChanged: (bool value) {

                },
                value: true,
              ),
            )
          ],
        ));
  }
}
