import 'package:flutter/material.dart';

class AutoComplete extends StatefulWidget {
  const AutoComplete({Key? key}) : super(key: key);

  @override
  _AutoCompleteState createState() => _AutoCompleteState();
}

class _AutoCompleteState extends State<AutoComplete> {
  List<String>? _listOption = [
    'angkasa',
    'bianglala',
    'cicak',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contoh Auto Complete"),
      ),
      body: Autocomplete<String>(
        optionsBuilder: (TextEditingValue textEditingValue) {
          if (textEditingValue.text == "") {
            return Iterable<String>.empty();
          }
          return _listOption!.where((String element) {
            return element.contains(textEditingValue.text);
          });
        },
        onSelected: (String selected) {
          print("Anda baru saja memilih $selected");
        },
      ),
    );
  }
}
