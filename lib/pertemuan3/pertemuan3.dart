import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan3/materi1.dart';
import 'package:mobile_flutter/pertemuan3/materi2.dart';
import 'package:mobile_flutter/pertemuan3/materi3.dart';

class Pertemuan3Page extends StatefulWidget {
  const Pertemuan3Page({Key? key}) : super(key: key);

  @override
  _Pertemuan3PageState createState() => _Pertemuan3PageState();
}

class _Pertemuan3PageState extends State<Pertemuan3Page> {
  @override
  Widget build(BuildContext context) {
    double size = 60;
    return Scaffold(
      appBar: AppBar(
        title: Text('Pertemuan 3'),),
      body: ListView(
        children: [
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => P3Materi1()),
              );
            },
            title: Text('Materi 1'),
            subtitle: Text('From, Textfield, checkbox, radiobutton, scrollview'),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Umam')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (route) => P3Materi2(),
                ),
              );
            },
            title: Text('Materi 2'),
            subtitle: Text('button (raised, flat, icon, inkwell)'),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Elfas')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
             Navigator.of(context).push(MaterialPageRoute(
               builder: (route) => Materi3(),
             ));
            },
            title: Text('Materi 3'),
            subtitle: Text(
              'Scroll View, List View, Grid View',
            ),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Rouf')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
        ],
      ),
    );
  }
}