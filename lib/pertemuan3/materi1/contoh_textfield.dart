import 'package:flutter/material.dart';

class TextFormFieldPage extends StatefulWidget {
  @override
  _TextFormFieldPageState createState() => _TextFormFieldPageState();
}

class _TextFormFieldPageState extends State<TextFormFieldPage> {
  String test = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("TextFormField"),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
            child: TextFormField(),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
            child: TextFormField(
              decoration: InputDecoration(
                  labelText: "Nama",
                  hintText: "e.g. Hint",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8)),
                  suffixIcon: Icon(Icons.close_rounded),
                  suffixText: "suffix",
                  helperText: "helper",
                  counterText: "counter",
                  errorText: "error",
                  icon: Text("Widget"),
                  prefixText: "prefix",
                  prefixIcon: Icon(Icons.email)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
            child: TextFormField(
              textAlign: TextAlign.center,
              obscureText: true,
              maxLength: 9,
              keyboardType: TextInputType.number,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
            child: TextFormField(
              maxLines: 3,
              onChanged: (value) {
                setState(() {
                  this.test = value;
                });
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
            child: Center(
                child: Text(
              this.test,
              textAlign: TextAlign.center,
            )),
          ),
        ],
      ),
    );
  }
}
