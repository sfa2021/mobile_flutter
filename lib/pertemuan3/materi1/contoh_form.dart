import 'package:flutter/material.dart';

class FormPage extends StatefulWidget {
  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  final _formKey = GlobalKey<FormState>();
  bool muncul = false;
  TextEditingController nameCont = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      appBar: AppBar(
//        title: Text("Form"),
//      ),
      body: Form(
        key: _formKey,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                controller: nameCont,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Field ini wajib diisi';
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    this.muncul = false;
                  });
                  if (_formKey.currentState!.validate()) {
                    setState(() {
                      this.muncul = true;
                    });
                  }
                },
                child: const Text('Proses'),
              ),
            ),
            if (this.muncul == true) Text("Hasil : ${this.nameCont.text}")
          ],
        ),
      ),
    );
  }
}
