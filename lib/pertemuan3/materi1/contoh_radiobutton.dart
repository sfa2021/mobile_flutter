import 'package:flutter/material.dart';

class RadioPage extends StatefulWidget {
  @override
  _RadioPageState createState() => _RadioPageState();
}

class _RadioPageState extends State<RadioPage> {
  List<data> listCheck = [
    data(Icons.save, "Programmer"),
    data(Icons.format_paint, "Designer"),
    data(Icons.music_note, "Composer"),
  ];

  String? _job;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("CheckboxListTile"),
      ),
      body: ListView.builder(
        itemCount: listCheck.length,
        itemBuilder: (_, int i){
          return RadioListTile(
            title: Text(this.listCheck[i].title),
            value: this.listCheck[i].title,
            groupValue: _job,
            activeColor: Colors.blue,
            onChanged: (String? value) {
              setState(() {
                this._job = value;
              });
            },
            secondary: Icon(this.listCheck[i].icon),
            controlAffinity: ListTileControlAffinity.trailing,
          );
        },
      ),
    );
  }
}

class data {
  IconData icon;
  String title;

  data(this.icon, this.title);
}
