import 'package:flutter/material.dart';

class CheckboxPage extends StatefulWidget {
  @override
  _CheckboxPageState createState() => _CheckboxPageState();
}

class _CheckboxPageState extends State<CheckboxPage> {
  List<data> listCheck = [
    data(Icons.save, "Programmer", false),
    data(Icons.format_paint, "Designer", false),
    data(Icons.music_note, "Composer", false),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("CheckboxListTile"),
      ),
      body: ListView.builder(
        itemCount: listCheck.length,
        itemBuilder: (_, int i){
          return CheckboxListTile(
            title: Text(this.listCheck[i].title),
            value: this.listCheck[i].cek,
            activeColor: Colors.blue,
            checkColor: Colors.white,
            onChanged: (bool? value) {
              setState(() {
                this.listCheck[i].cek = value!;
              });
            },
            selected: this.listCheck[i].cek,
            secondary: Icon(this.listCheck[i].icon),
            controlAffinity: ListTileControlAffinity.trailing,
          );
        },
      ),
    );
  }
}

class data {
  IconData icon;
  String title;
  bool cek;

  data(this.icon, this.title, this.cek);
}
