import 'package:flutter/material.dart';

class ContohScrollview extends StatefulWidget {
  @override
  _ContohScrollviewState createState() => _ContohScrollviewState();
}

class _ContohScrollviewState extends State<ContohScrollview> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Scroll View"),
      ),
      body: Container()
    );
  }
}
