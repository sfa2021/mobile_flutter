import 'package:flutter/material.dart';

class CustomeButton extends StatelessWidget {
  double buttonH = 50, buttonW = 150;
  String? title;
  IconData? icon;
  void Function()? press;
  Color? warna;

  CustomeButton({this.title, this.icon, this.press, this.warna});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: this.press ?? (){},
      child: Container(
        margin: EdgeInsets.all(10),
        height: buttonH,
        width: buttonW,
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
              offset: Offset(0, 20), blurRadius: 30, color: Colors.black12),
        ], color: Colors.white, borderRadius: BorderRadius.circular(32)),
        child: Row(
          children: [
            Container(
              height: buttonH,
              width: buttonW - 40,
              child: Center(
                  child: Text(
                this.title!,
                style: TextStyle(color: Colors.white, fontSize: 18),
              )),
              decoration: BoxDecoration(
                  color: this.warna!,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(92),
                    topLeft: Radius.circular(92),
                    bottomRight: Radius.circular(200),
                  )),
            ),
            Icon(
              this.icon!,
              size: 25,
              color: this.warna!,
            )
          ],
        ),
      ),
    );
  }
}
