import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan5/materi2/custome_appbar.dart';
import 'package:mobile_flutter/pertemuan5/materi3/contoh_getx.dart';
import 'package:mobile_flutter/pertemuan5/materi3/contoh_nostate.dart';
import 'package:mobile_flutter/pertemuan5/materi3/contoh_stateful.dart';
import 'package:mobile_flutter/pertemuan5/materi3/contoh_stateless.dart';

class P5Materi3 extends StatefulWidget {
  @override
  _P5Materi3State createState() => _P5Materi3State();
}

class _P5Materi3State extends State<P5Materi3> {
  Widget _listTile(BuildContext context, String title, Widget page, {String? subtitle}) {
    return ListTile(
      isThreeLine: true,
      title: Text(title),
      subtitle: Text(subtitle ?? ''),
      trailing: Icon(Icons.arrow_forward_ios_rounded),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (buildContext) {
            return page;
          }),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF3F5F7),
      appBar: CustomeAppBar(title: "Materi 3 - Pengenalan State"),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _listTile(
              context,
              "Contoh Stateless",
              ContohStateless(),
            ),
            _listTile(
              context,
              "Contoh Stateful",
              ContohStateless(),
            ),
            _listTile(
              context,
              "Contoh Perbandingan State",
              Scaffold(
                appBar: AppBar(title: Text("Contoh Perbandingan State")),
                body: Column(
                  children: [
                    Expanded(child: ContohStateless()),
                    Expanded(child: ContohStateful()),
                  ],
                ),
              ),
            ),
            _listTile(
              context,
              "Contoh tanpa State Management",
              ContohNoState(),
            ),
            _listTile(
              context,
              "Contoh State Management",
              ContohGetX(),
              subtitle: "Menggunakan GetX",
            ),
          ],
        ),
      ),
    );
  }
}
