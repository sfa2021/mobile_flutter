import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan5/materi1.dart';
import 'package:mobile_flutter/pertemuan5/materi2.dart';
import 'package:mobile_flutter/pertemuan5/materi3.dart';
export 'package:mobile_flutter/pertemuan5/materi3.dart';

class Pertemuan5Page extends StatefulWidget {
  const Pertemuan5Page({Key? key}) : super(key: key);

  @override
  _Pertemuan5PageState createState() => _Pertemuan5PageState();
}

class _Pertemuan5PageState extends State<Pertemuan5Page> {
  double size = 60;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pertemuan 5'),),
      body: ListView(
        children: [
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => P5Metari1()),
              );
            },
            title: Text('Materi 1'),
            subtitle: Text('dialog, bottomsheet, snackbar, taoast'),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Elfas')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => P5Metari2()),
              );
            },
            title: Text('Materi 2'),
            subtitle: Text('custome widget'),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Umam')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (route) => P5Materi3(),
              ));
            },
            title: Text('Materi 3'),
            subtitle: Text(
              'Pengenalan State (State Management)',
            ),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Rouf')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
        ],
      ),
    );
  }
}
