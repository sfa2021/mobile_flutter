import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan5/materi2/custome_PopUp.dart';
import 'package:mobile_flutter/pertemuan5/materi2/custome_appbar.dart';
import 'package:mobile_flutter/pertemuan5/materi2/custome_button.dart';
import 'package:mobile_flutter/pertemuan5/materi2/custome_card1.dart';
import 'package:mobile_flutter/pertemuan5/materi2/custome_card2.dart';
import 'package:mobile_flutter/pertemuan5/materi2/custome_card3.dart';

class P5Metari2 extends StatefulWidget {
  @override
  _P5Metari2State createState() => _P5Metari2State();
}

class _P5Metari2State extends State<P5Metari2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF3F5F7),
      appBar:
          CustomeAppBar(
        title: "Custome App Bar",
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                CustomeButton(
                  title: "Kamera",
                  icon: Icons.camera_alt,
                  press: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return CustomePopUp(
                            warna: Colors.red,
                            icon: Icons.camera_alt,
                            isi: "Gagal membukan kamera",
                          );
                        });
                  },
                  warna: Colors.red,
                ),
                CustomeButton(
                  title: "Tambah",
                  icon: Icons.person_add,
                  press: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return CustomePopUp(
                            warna: Colors.yellow,
                            icon: Icons.warning,
                            isi: "Caming soon",
                          );
                        });
                  },
                  warna: Colors.green,
                ),
              ],
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10, vertical: 10),
                    child: CustomeCard3(
                      imgPath:
                          "https://raw.githubusercontent.com/rajayogan/flutterui---coffeebrew/master/assets/coffee4.png",
                      price: "150",
                      bgColor: Color(0xFFFCC07E),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10, vertical: 10),
                    child: CustomeCard3(
                      imgPath:
                          "https://raw.githubusercontent.com/rajayogan/flutterui---coffeebrew/master/assets/coffee2.png",
                      price: "200",
                      bgColor: Color(0xFF57CFEA),
                    ),
                  ),
                ],
              ),
            ),
            CustomeCard1(
              name: "St. Mark\'s Basilica",
              imageUrl:
                  "https://raw.githubusercontent.com/MarcusNg/flutter_travel_ui/master/assets/images/stmarksbasilica.jpg",
              price: 30,
              rating: 4,
              type: "Sightseeing Tour",
              startTimes: "9:00 am",
              endTimes: "11:00 am",
            ),
            CustomeCard2(
                imgPath:
                    'https://raw.githubusercontent.com/rajayogan/flutterui-coffeeshop/master/assets/starbucks.png',
                coffeeName: 'Caffe Misto',
                shopName: 'Coffeeshop',
                description:
                    'Our dark, rich espresso balanced with steamed milk and a light layer of foam',
                price: '\$4.99',
                isFavorite: false),
          ],
        ),
      ),
    );
  }
}
