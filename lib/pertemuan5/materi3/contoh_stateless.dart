import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan5/materi3/model_product.dart';

class ContohStateless extends StatelessWidget {
  ContohStateless({Key? key}) : super(key: key);
  final int nilai = 0;
  final Data data = Data();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("SFA - Contoh Stateless")),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh),
        onPressed: () {
          // nilai++;
          data.nilai++;
          // setState(() {});

          if (data.nilai >= 3) {
            showDialog(
              context: context,
              builder: (bc) => AlertDialog(
                title: Text("Alert ${data.nilai}"),
              ),
            );
            // ).then((value) => nilai = 0);
            // nilai = 0;
          } else {
            data.nilai++;
          }
        },
      ),
      body: Center(
          child: Column(
        children: [
          Text("nilai $nilai"),
          Text("nilai ${data.nilai}"),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 50),
            child: Text(
              "Nilai tidak berubah karena tidak ada state yang berubah atau build tidak di eksekusi ulang",
            ),
          ),
        ],
      )),
    );
  }
}
