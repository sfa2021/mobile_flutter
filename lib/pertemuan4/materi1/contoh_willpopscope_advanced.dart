import 'package:flutter/material.dart';

class ContohPopScope extends StatefulWidget {
  const ContohPopScope({Key? key}) : super(key: key);

  @override
  _ContohPopScopeState createState() => _ContohPopScopeState();
}

class _ContohPopScopeState extends State<ContohPopScope> {
  GlobalKey<NavigatorState>? _key = GlobalKey();
  late String routeName;

  @override
  void initState() {
    super.initState();
    setState(() {
      routeName = "/";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contoh WillPopScope"),
      ),
      body: WillPopScope(
        onWillPop: () async {
          // if (_key!.currentState!.canPop()) {
          //   print("WillPopScope is running with ${ModalRoute.of(context)!.settings.name}");
          //
          //   if(ModalRoute.of(context)!.settings.name == '/halamandua') {
          //     await showDialog(
          //         context: context,
          //         builder: (BuildContext ctx) {
          //           return AlertDialog(
          //             title: Text("Apakah ingin keluar dari halaman ini?"),
          //             actions: [
          //               RaisedButton(
          //                 onPressed: () {
          //                   Navigator.pop(context);
          //                   _key!.currentState!.pop();
          //                 },
          //                 child: Text("Ya"),
          //               ),
          //               RaisedButton(
          //                 onPressed: () {
          //                   Navigator.pop(context);
          //                 },
          //                 child: Text("Tidak"),
          //               )
          //             ],
          //           );
          //         });
          //   }
          //
          //   else {
          //     _key!.currentState!.pop();
          //   }
          //   return false;
          // }

          if (_key!.currentState!.canPop()) {
            print('routeName $routeName');
            if (routeName == '/halamantiga') {
              print('masuk if halaman ketiga');
              setState(() {
                routeName = '/halamandua';
              });
              // _key!.currentState!.popAndPushNamed('/halamandua');
              _key!.currentState!.pop();
            } else if (routeName == '/halamandua') {
              print('masuk if halaman dua');

              await showDialog(
                  context: context,
                  builder: (BuildContext ctx) {
                    return AlertDialog(
                      title: Text("Apakah ingin keluar dari halaman ini?"),
                      actions: [
                        RaisedButton(
                          onPressed: () {
                            Navigator.pop(context);
                            _key!.currentState!.pop();
                          },
                          child: Text("Ya"),
                        ),
                        RaisedButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text("Tidak"),
                        )
                      ],
                    );
                  });
            } else {
              _key!.currentState!.pop();
            }

            return false;
          }

          return true;
        },
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          home: Navigator(
            initialRoute: '/',
            key: _key,
            onGenerateRoute: (RouteSettings settings) {
              print('route name : ${settings.name!}');

              late WidgetBuilder builder;

              if (settings.name! == '/') {
                builder = (BuildContext ctx) => HalamanSatu();
              } else if (settings.name! == '/halamandua') {
                builder = (BuildContext ctx) => HalamanDua();

                setState(() {
                  routeName = '${settings.name!}';
                });
              } else if (settings.name! == '/halamantiga') {
                builder = (BuildContext ctx) => HalamanTiga();

                setState(() {
                  routeName = '${settings.name!}';
                });
              }

              return MaterialPageRoute(builder: builder, settings: settings);
            },
          ),
          routes: {
            '/halamandua': (BuildContext ctx) => HalamanDua(),
            '/halamantiga': (BuildContext ctx) => HalamanTiga(),
          },
        ),
      ),
    );
  }
}

class HalamanSatu extends StatelessWidget {
  const HalamanSatu({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: RaisedButton(
            child: Text('Halaman 1 ke Halaman Selanjutnya'),
            onPressed: () {
              Navigator.of(context).pushNamed('/halamandua');
            },
          ),
        ),
      ),
    );
  }
}

class HalamanDua extends StatelessWidget {
  const HalamanDua({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: RaisedButton(
            child: Text('halaman 2 ke  Halaman Selanjutnya'),
            onPressed: () {
              Navigator.of(context).pushNamed('/halamantiga');
            },
          ),
        ),
      ),
    );
  }
}

class HalamanTiga extends StatelessWidget {
  const HalamanTiga({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: Text('Ini halaman 3'),
        ),
      ),
    );
  }
}
