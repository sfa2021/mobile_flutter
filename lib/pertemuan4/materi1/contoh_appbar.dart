import 'package:flutter/material.dart';

class ContohAppBar extends StatefulWidget {
  const ContohAppBar({Key? key}) : super(key: key);

  @override
  _ContohAppBarState createState() => _ContohAppBarState();
}

class _ContohAppBarState extends State<ContohAppBar> {
  List<String> _listMenuPopUp = ["Pengaturan", "Keluar"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: customAppbar(),
      appBar: instaBar(),
      // appBar: AppBar(
      //   title: Text("Contoh Appbar"),
      // ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: AppBar(
                backgroundColor: Colors.amberAccent,
                elevation: 3,
                title: const Text("Contoh"),
                leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    print("On Pressed");
                  },
                ),
                // leading: Builder(builder: (BuildContext _) {
                //   // berdasarkan kondisi tertentu
                //   if (true) {
                //     return Container();
                //   }
                // }),
                actions: [
                  Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Icon(Icons.search),
                  ),
                  Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Icon(Icons.add_link),
                  ),
                  PopupMenuButton(onSelected: (String value) {
                    switch (value) {
                      case 'Pengaturan':
                        break;
                      case 'Keluar':
                        break;
                    }
                  }, itemBuilder: (BuildContext _) {
                    return _listMenuPopUp
                        .map((e) =>
                            PopupMenuItem<String>(value: e, child: Text(e)))
                        .toList();
                  })
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: AppBar(
                title: Text("Contoh Appbar Radius"),
                backgroundColor: Colors.black,
                shape: RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.vertical(bottom: Radius.circular(23))),
              ),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Text("Contoh Appbar Gradient"))
          ],
        ),
      ),
    );
  }

  AppBar customAppbar() {
    return AppBar(
      title: Text("Contoh"),
      flexibleSpace: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: <Color>[Colors.black, Colors.amber])),
      ),
    );
  }

  AppBar instaBar() {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0,
      centerTitle: true,
      title: Container(
        width: 143,
        child: Image.asset('assets/images/instagram.png'),
      ),
      leading: Container(
          child: Icon(
        Icons.menu,
        color: Colors.black54,
      )),
      actions: [
        Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: Container(
                margin: EdgeInsets.all(10),
                child: Icon(
                  Icons.notifications_none_outlined,
                  color: Colors.black54,
                  size: 28,
                ),
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: Padding(
                padding: const EdgeInsets.only(left: 25, top: 10),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.redAccent,
                      borderRadius: BorderRadius.all(Radius.circular(50))
                  ),
                  width: 15,
                  height: 15,
                  child: Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Center(
                      child: Text("2", style: TextStyle(
                          color: Colors.white,
                          fontSize: 10
                      ),),
                    ),
                  ),
                ),
              ),
            ),
          ]
        )
      ],
    );
  }
}
