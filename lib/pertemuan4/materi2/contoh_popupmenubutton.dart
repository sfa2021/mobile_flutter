/*
 * Create by roufroufrouf on 2021
 * Email: roufroufrouf@gmail.com
 * Last modified 9/2/21, 4:28 PM
 */

import 'package:flutter/material.dart';

class ContohPopupMenuButton extends StatefulWidget {
  const ContohPopupMenuButton({Key? key}) : super(key: key);

  @override
  _ContohPopupMenuButtonState createState() => _ContohPopupMenuButtonState();
}

class _ContohPopupMenuButtonState extends State<ContohPopupMenuButton> {
  String bahasa = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contoh PopupMenuButton"),
      ),
      body: Column(
        children: [
          Text("Bahasa"),
          PopupMenuButton<String>(
            // child: Text("Bahasa"),
            icon: Icon(Icons.more),
            initialValue: bahasa,
            // enabled: false,
            tooltip: "Pilih bahasa",
            elevation: 24,
            color: Colors.blue[100],
            // padding: EdgeInsets.all(0),
            offset: Offset(50.0, 150.0),
            itemBuilder: (buildContext) {
              return [
                PopupMenuItem(child: Text("Indonesia"), value: "ID"),
                PopupMenuItem(child: Text("Japan"), value: "JP"),
                PopupMenuItem(child: Text("Korea"), value: "KR"),
              ];
            },
            onCanceled: () {
              print("gajadi");
            },
            onSelected: (value) {
              bahasa = value;
              print("pilih bahasa $value");
              setState(() {});
            },
          ),
        ],
      ),
    );
  }
}
