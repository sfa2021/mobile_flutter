/*
 * Create by roufroufrouf on 2021
 * Email: roufroufrouf@gmail.com
 * Last modified 9/2/21, 9:15 PM
 */

import 'package:flutter/material.dart';

class ContohDrawerFix extends StatefulWidget {
  const ContohDrawerFix({Key? key}) : super(key: key);

  @override
  _ContohDrawerFixState createState() => _ContohDrawerFixState();
}

class _ContohDrawerFixState extends State<ContohDrawerFix> {
  Widget _contohDrawerFix() {
    return Drawer(
      child: Column(
        children: [
          DrawerHeader(
            child: Container(
              color: Colors.green,
            ),
          ),
          Expanded(
            child: ListView(
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              children: [
                ListTile(
                  leading: Icon(Icons.people),
                  title: Text("Daftar Pelanggan"),
                ),
                ListTile(
                  leading: Icon(Icons.inbox),
                  title: Text("Inbox"),
                ),
                ...List.generate(
                  15,
                  (index) => ListTile(
                    leading: Icon(Icons.inbox),
                    title: Text("Inbox"),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
                Container(
                  child: Text("Footer scroll"),
                ),
              ],
            ),
          ),
          Container(
            child: Text("Footer Fix"),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Contoh Drawer Fix")),
      body: Center(
        child: ElevatedButton.icon(
          icon: Icon(Icons.arrow_back_ios_rounded),
          label: Text("Kembali"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      endDrawer: _contohDrawerFix(),
      onDrawerChanged: (isOpen) {
        print("status drawer $isOpen");
      },
    );
  }
}
