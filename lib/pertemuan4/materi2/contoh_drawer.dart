/*
 * Create by roufroufrouf on 2021
 * Email: roufroufrouf@gmail.com
 * Last modified 9/2/21, 9:15 PM
 */

import 'package:flutter/material.dart';

class ContohDrawer extends StatefulWidget {
  const ContohDrawer({Key? key}) : super(key: key);

  @override
  _ContohDrawerState createState() => _ContohDrawerState();
}

class _ContohDrawerState extends State<ContohDrawer> {
  Widget _contohDrawer() {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            child: Container(
              color: Colors.green,
            ),
          ),
          ListTile(
            leading: Icon(Icons.people),
            title: Text("Daftar Pelanggan"),
          ),
          ListTile(
            leading: Icon(Icons.inbox),
            title: Text("Inbox"),
          ),
          ...List.generate(
            15,
            (index) => ListTile(
              leading: Icon(Icons.inbox),
              title: Text("Inbox"),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Contoh Drawer")),
      body: Center(
        child: ElevatedButton.icon(
          icon: Icon(Icons.arrow_back_ios_rounded),
          label: Text("Kembali"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      drawer: _contohDrawer(),
      onDrawerChanged: (isOpen) {
        print("status drawer $isOpen");
      },
    );
  }
}
