import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan4/materi2/contoh_double_drawer.dart';
import 'package:mobile_flutter/pertemuan4/materi2/contoh_drawer.dart';
import 'package:mobile_flutter/pertemuan4/materi2/contoh_drawer_fix.dart';
import 'package:mobile_flutter/pertemuan4/materi2/contoh_popupmenubutton.dart';
import 'package:mobile_flutter/pertemuan4/materi2/demo_drawer.dart';

class P4Materi2 extends StatefulWidget {
  const P4Materi2({Key? key}) : super(key: key);

  @override
  _P4Materi2State createState() => _P4Materi2State();
}

class _P4Materi2State extends State<P4Materi2> {
  Widget list({required String title, required Widget widget}) {
    return ListTile(
      title: Text('$title'),
      trailing: Icon(Icons.arrow_right),
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(builder: (route) => widget),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Materi 2")),
        body: ListView(
          children: [
            list(
              title: "Contoh PopupMenuButton",
              widget: ContohPopupMenuButton(),
            ),
            list(
              title: "Contoh Drawer",
              widget: ContohDrawer(),
            ),
            list(
              title: "Contoh Double Drawer",
              widget: ContohDoubleDrawer(),
            ),
            list(
              title: "Contoh Drawer Fix",
              widget: ContohDrawerFix(),
            ),
            list(
              title: "Demo Drawer",
              widget: DemoDrawer(),
            ),
          ],
        ));
  }
}
