import 'package:flutter/material.dart';

class TabBarViewPage extends StatefulWidget {
  @override
  _TabBarViewPageState createState() => _TabBarViewPageState();
}

class _TabBarViewPageState extends State<TabBarViewPage> {
  List<Widget> listScreen = [
    Center(
      child: Text(
        "Kamera",
        style: TextStyle(fontSize: 30),
      ),
    ),
    Center(
      child: Text(
        "Chat List",
        style: TextStyle(fontSize: 30),
      ),
    ),
    Center(
      child: Text(
        "Status List",
        style: TextStyle(fontSize: 30),
      ),
    ),
    Center(
      child: Text(
        "Call List",
        style: TextStyle(fontSize: 30),
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text("Tab Bar"),
          bottom: TabBar(
//            tabs: [
//              Tab(
//                icon: Icon(Icons.camera_alt),
//              ),
//              Tab(
//                text: "CHATS",
//              ),
//              Tab(
//                child: Row(
//                  children: [
//                    Text("STATUS"),
//                    Text("*"),
//                  ],
//                ),
//              ),
//              Tab(
//                text: "CALLS",
//              ),
//            ],
              isScrollable: true,
              labelPadding: EdgeInsets.symmetric(horizontal: 5.0),
              tabs: [
                Container(
                  margin: EdgeInsets.only(bottom: 5),
                  width: 30,
                  child: Icon(Icons.camera_alt),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 5),
                  width: (MediaQuery.of(context).size.width-70)/3,
                  child: Center(child: Text("CHATS")),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 5),
                  width: (MediaQuery.of(context).size.width-70)/3,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text("STATUS"),
                        Text("*"),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 5),
                  width: (MediaQuery.of(context).size.width-70)/3,
                  child: Center(child: Text("CALLS")),
                ),
              ],
          ),
        ),
        body: TabBarView(
          children: listScreen,
        ),
      ),
    );
  }
}
