import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan3/materi1/contoh_form.dart';

class BottomNavBarPage extends StatefulWidget {
  @override
  _BottomNavBarPageState createState() => _BottomNavBarPageState();
}

class _BottomNavBarPageState extends State<BottomNavBarPage> {
  List<Widget> listScreen = [
//    Text("Screen 1", style: TextStyle(fontSize: 30),),
    FormPage(),
    Text(
      "Screen 2",
      style: TextStyle(fontSize: 30),
    ),
    Text(
      "Screen 3",
      style: TextStyle(fontSize: 30),
    ),
  ];

  int currentScreen = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Navigation Bar"),
      ),
      body: listScreen.elementAt(currentScreen),
      bottomNavigationBar:
      BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(icon: Icon(Icons.search), label: 'Explore'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Account'),
        ],
        currentIndex: this.currentScreen,
        selectedItemColor: Colors.amber,
        onTap: (int index) {
          setState(() {
            this.currentScreen = index;
          });
        },
      ),
    );
  }
}
