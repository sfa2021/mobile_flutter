import 'package:flutter/material.dart';

class ContohNotification extends StatefulWidget {
  const ContohNotification({Key? key}) : super(key: key);

  @override
  _ContohNotificationState createState() => _ContohNotificationState();
}

class _ContohNotificationState extends State<ContohNotification> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contoh Notification'),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("1. Untuk membuat notifikasi untuk SmartPhone, pastikan Project telah terhubung dengan Firebase."),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("2. Untuk Project Ini, gunakan File main_firebase.dart sebagai main project untuk mencoba notifikasi. Setelah di-set, compile ulang."),
          )
        ],
      )
    );
  }
}
