import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan6/materi1/contoh_navigasi.dart';
import 'package:mobile_flutter/pertemuan6/materi1/contoh_state.dart';

class P6Materi1 extends StatefulWidget {
  const P6Materi1({Key? key}) : super(key: key);

  @override
  _P6Materi1State createState() => _P6Materi1State();
}

class _P6Materi1State extends State<P6Materi1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pertemuan 6 Materi 1'),
      ),
      body: Column(
        children: [
          list(
              title: 'State',
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx) => ContohState()));
              }
          ),
          list(
              title: 'Navigasi',
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx) => ContohNavigasi()));
              }
          )
        ],
      ),
    );
  }

  Widget list({String? title, GestureTapCallback? onTap}) {
    return ListTile(
      title: Text('$title'),
      trailing: Icon(Icons.arrow_right),
      onTap: onTap,
    );
  }
}
