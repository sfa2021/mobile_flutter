import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan6/P6Materi1.dart';
import 'package:mobile_flutter/pertemuan6/materi1/contoh_navigasi.dart';

class Pertemuan6Page extends StatefulWidget {
  const Pertemuan6Page({Key? key}) : super(key: key);

  @override
  _Pertemuan6PageState createState() => _Pertemuan6PageState();
}

class _Pertemuan6PageState extends State<Pertemuan6Page> {
  double size = 60;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pertemuan 6'),),
      body: ListView(
        children: [
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => P6Materi1()),
              );
            },
            title: Text('Materi 1'),
            subtitle: Text('State, Navigation'),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Elfas')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              // Navigator.of(context).push(
              //   MaterialPageRoute(builder: (route) => P6Materi2()),
              // );
            },
            title: Text('Materi 2'),
            subtitle: Text('Provider'),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Umam')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
        ],
      ),
    );
  }
}