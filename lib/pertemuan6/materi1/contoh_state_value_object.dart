import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan6/materi1/NamaOrang.dart';

class ContohStateUbahObject extends StatefulWidget {
  const ContohStateUbahObject({Key? key}) : super(key: key);

  @override
  _ContohStateUbahObjectState createState() => _ContohStateUbahObjectState();
}

class _ContohStateUbahObjectState extends State<ContohStateUbahObject> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController? _editControllerNamaDepan = TextEditingController();
  final TextEditingController? _editControllerNamaBelakang = TextEditingController();
  NamaOrang? _namaOrang;

  @override
  void initState() {
    super.initState();
    _namaOrang = NamaOrang(namaDepan: "", namaBelakang: "");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ubah state nilai class objek'),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _editControllerNamaDepan!,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _editControllerNamaBelakang!,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        _namaOrang = NamaOrang(
                            namaDepan: _editControllerNamaDepan!.text,
                            namaBelakang: _editControllerNamaBelakang!.text);
                      });
                    },
                    child: Text('Simpan Data')),
              ),
              _namaOrang!.namaDepan!.isEmpty
                  ? Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Nama Depan Belum Diisi'),
                    )
                  : Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Nama Depan : ${_namaOrang!.namaDepan!}'),
                    ),
              _namaOrang!.namaBelakang!.isEmpty
                  ? Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Nama Depan Belum Diisi'),
                    )
                  : Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Nama Depan : ${_namaOrang!.namaBelakang!}'),
                    )
            ],
          ),
        ),
      ),
    );
  }
}
