import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan6/materi1/contoh_state_change_value.dart';
import 'package:mobile_flutter/pertemuan6/materi1/contoh_state_list.dart';
import 'package:mobile_flutter/pertemuan6/materi1/contoh_state_value_object.dart';

class ContohState extends StatefulWidget {
  const ContohState({Key? key}) : super(key: key);

  @override
  _ContohStateState createState() => _ContohStateState();
}

// contoh setState change value and object value
// contoh setState new value (string) and object
// contoh setState add new array

class _ContohStateState extends State<ContohState> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contoh State'),
      ),
      body: Column(
        children: [
          list(
            title: 'Contoh setState perubahan value',
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx) => ContohStateUbahNilai()));
            }
          ),
          list(
              title: 'Contoh setState perubahan object model)',
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx) => ContohStateUbahObject()));
              }
          ),
          list(
            title: 'Contoh setState perubahan array of object',
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx) => ContohStateList()));
            }
          )
        ],
      ),
    );
  }

  Widget list({String? title, GestureTapCallback? onTap}) {
    return ListTile(
      title: Text('$title'),
      trailing: Icon(Icons.arrow_right),
      onTap: onTap,
    );
  }
}
