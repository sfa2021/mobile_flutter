import 'package:flutter/material.dart';

class ContohNavigasi extends StatefulWidget {
  const ContohNavigasi({Key? key}) : super(key: key);

  @override
  _ContohNavigasiState createState() => _ContohNavigasiState();
}

class _ContohNavigasiState extends State<ContohNavigasi> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/' : (BuildContext context) => MainPage(),
        'halamansatu': (BuildContext context) => HalamanSatu(),
        'halamandua': (BuildContext context) {
          final String args = ModalRoute.of(context)!.settings.arguments as String;
          return HalamanDua(param: args);
        }
      },
      onGenerateRoute: (RouteSettings? settings) {
        print('nama route : ${settings!.name}');

        if(settings.name == SubHalamanSatu.routeName) {
          return MaterialPageRoute(builder: (BuildContext context) => SubHalamanSatu());
        }

        return MaterialPageRoute(builder: (BuildContext context) => HalamanRandom());
      },
      initialRoute: '/',
    );
  }
}

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {

  final TextEditingController? _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contoh Navigasi"),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.center,
              child: TextButton(
                child: Text("Menuju Halaman Satu"),
                onPressed: () {
                  Navigator.pushNamed(context, 'halamansatu');
                },
              ),
            ),

            Container(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: TextFormField(
                  controller: _controller,
                ),
              ),
            ),

            Container(
              alignment: Alignment.center,
              child: TextButton(
                child: Text("Menuju Halaman Dua"),
                onPressed: () {
                  final String nama = _controller!.text;
                  Navigator.pushNamed(context, 'halamandua', arguments: nama);
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}


class HalamanSatu extends StatefulWidget {
  const HalamanSatu({Key? key}) : super(key: key);

  @override
  _HalamanSatuState createState() => _HalamanSatuState();
}

class _HalamanSatuState extends State<HalamanSatu> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop();
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text("Halaman Satu"),
        ),
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextButton(
                  onPressed: () {
                    // Navigator.pushNamed(context, SubHalamanSatu.routeName);
                    Navigator.pushReplacementNamed(context, SubHalamanSatu.routeName);
                    // Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => SubHalamanSatu(param: "Zayed Elfasa",)));
                  },
                  child: Text("Klik ini untuk Menuju Sub Halaman 1"))
            ],
          ),
        ),
      ),
    );
  }
}

class SubHalamanSatu extends StatefulWidget {
  const SubHalamanSatu({Key? key}) : super(key: key);

  static const routeName = "subHalamanSatu";

  @override
  _SubHalamanSatuState createState() => _SubHalamanSatuState();
}

class _SubHalamanSatuState extends State<SubHalamanSatu> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Sub Halaman Satu"),
      ),
      body: Container(
        alignment: Alignment.center,
        child: TextButton(
          child: Text("Kembali Ke Halaman Sebelumnya"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}

class HalamanDua extends StatefulWidget {
  const HalamanDua({
    Key? key,
    this.param
  }) : super(key: key);

  final String? param;

  @override
  _HalamanDuaState createState() => _HalamanDuaState();
}

class _HalamanDuaState extends State<HalamanDua> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contoh Passing Arguments'),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('Ini Halaman 2'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('Isi Argument : ${widget.param}'),
            )
          ],
        ),
      ),
    );
  }
}

class HalamanRandom extends StatelessWidget {
  const HalamanRandom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: Container(
        alignment: Alignment.center,
        child: Text('Halaman tidak ditemukan'),
      ),
    );
  }
}

