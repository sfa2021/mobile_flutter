import 'package:flutter/material.dart';

class ContohStateUbahNilai extends StatefulWidget {
  const ContohStateUbahNilai({Key? key}) : super(key: key);

  @override
  _ContohStateUbahNilaiState createState() => _ContohStateUbahNilaiState();
}

class _ContohStateUbahNilaiState extends State<ContohStateUbahNilai> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController? _textEditingControllerNama = TextEditingController();
  String? _nama = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ubah state tipe data'),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _textEditingControllerNama!,
                  onChanged: (String value) {
                    setState(() {
                      _nama = value;
                    });
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Output : $_nama'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
