import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan5/materi2/custome_appbar.dart';
import 'package:mobile_flutter/pertemuan7/materi3/contoh_asynchronous.dart';
import 'package:mobile_flutter/pertemuan7/materi3/contoh_login.dart';
import 'package:mobile_flutter/pertemuan7/materi3/contoh_persistence.dart';
export 'package:mobile_flutter/pertemuan7/materi3/contoh_persistence.dart';

class P7Materi3 extends StatefulWidget {
  @override
  _P7Materi3State createState() => _P7Materi3State();
}

class _P7Materi3State extends State<P7Materi3> {
  Widget _listTile(BuildContext context, String title, Widget page, {String? subtitle}) {
    return ListTile(
      isThreeLine: true,
      title: Text(title),
      subtitle: Text(subtitle ?? ''),
      trailing: Icon(Icons.arrow_forward_ios_rounded),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (buildContext) {
            return page;
          }),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF3F5F7),
      appBar: CustomeAppBar(title: "Materi 3 - Async, Future, Data Persistence"),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _listTile(
              context,
              "Contoh Asynchronous",
              ContohAsynchronous(),
            ),
            _listTile(
              context,
              "Pengenalan Data Persistence",
              ContohPersistence(),
              subtitle: "Shared Preferences vs Get Storage"
            ),
            _listTile(
              context,
              "Contoh Login",
              ContohLogin(),
            ),
          ],
        ),
      ),
    );
  }
}
