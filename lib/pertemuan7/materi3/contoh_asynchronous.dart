import 'package:flutter/material.dart';

class ContohAsynchronous extends StatefulWidget {
  const ContohAsynchronous({Key? key}) : super(key: key);

  @override
  _ContohAsynchronousState createState() => _ContohAsynchronousState();
}

class _ContohAsynchronousState extends State<ContohAsynchronous> {
  bool? loading;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Perhatian")),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 50),
              child: Text(
                "Pengenalan asynchronous silakan cek kode pada folder /bin dalam project ini",
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 24),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  loading = true;
                });
                Future.delayed(Duration(seconds: 2)).whenComplete(() {
                  setState(() {
                    loading = false;
                  });
                });
              },
              child: Text("Contoh Loading Async 1"),
            ),
            SizedBox(height: 24),
            ElevatedButton(
              onPressed: () async {
                setState(() {
                  loading = true;
                });
                await Future.delayed(Duration(seconds: 2));
                setState(() {
                  loading = false;
                });
              },
              child: Text("Contoh Loading Async 2"),
            ),
            if (loading == true)
              SizedBox(height: 24),
            if (loading == true)
              CircularProgressIndicator(),
          ],
        ),
      ),
    );
  }
}
