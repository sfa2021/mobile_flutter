import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile_flutter/pertemuan7/materi3/home_page.dart';
import 'package:mobile_flutter/pertemuan7/materi3/login_page.dart';

class ContohLogin extends StatelessWidget {
  const ContohLogin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("FirstInit")),
      body: Center(
        child: ElevatedButton(
          onPressed: () async {
            await GetStorage.init();
            Widget next = LoginPage();
            GetStorage storage = GetStorage();
            String? token = storage.read("token");
            if (token?.isNotEmpty ?? false) {
              next = HomePage();
            }
            Navigator.push(
              context,
              MaterialPageRoute(builder: (_) => next),
            );
          },
          child: Text("Check"),
        ),
      ),
    );
  }
}
