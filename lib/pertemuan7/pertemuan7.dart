import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan7/materi1.dart';
import 'package:mobile_flutter/pertemuan7/materi3.dart';
import 'package:mobile_flutter/pertemuan7/materi3/login_view.dart';
export 'package:mobile_flutter/pertemuan7/materi3.dart';

class Pertemuan7Page extends StatefulWidget {
  const Pertemuan7Page({Key? key}) : super(key: key);

  @override
  _Pertemuan7PageState createState() => _Pertemuan7PageState();
}

class _Pertemuan7PageState extends State<Pertemuan7Page> {
  double size = 60;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pertemuan 7'),),
      body: ListView(
        children: [
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => P7Materi1()),
              );
            },
            title: Text('Materi 1'),
            subtitle: Text('JSON, Model, Map, DIO, interceptor, upload'),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Elfas')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (route) => LoginView()),
              );
            },
            title: Text('Materi 2'),
            subtitle: Text('HTTP + Upload'),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Umam')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (route) => P7Materi3(),
              ));
            },
            title: Text('Materi 3'),
            subtitle: Text(
              'Asynchronous, Data Persistence',
            ),
            leading: Container(
              width: size,
              height: size,
              child: Center(child: Text('Rouf')),
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
        ],
      ),
    );
  }
}
