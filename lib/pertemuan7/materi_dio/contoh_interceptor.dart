import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class ContohDioInterceptor extends StatefulWidget {
  const ContohDioInterceptor({ Key? key }) : super(key: key);

  @override
  _ContohDioInterceptorState createState() => _ContohDioInterceptorState();
}

class _ContohDioInterceptorState extends State<ContohDioInterceptor> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Interceptor"),
      ),
      body: Container(
        child: ElevatedButton(onPressed: () {
          this._getNews();
        }, child: Text("GET NEWS!")),
      ),
    );
  }

  Future<void> _getNews() async {
    // input code here...
    String token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvc2ZhLmZvcmNhcG9zLnh5elwvYXBpXC9hY2NvdW50XC9sb2dpbiIsImlhdCI6MTYzMTg4NzE0MSwibmJmIjoxNjMxODg3MTQxLCJqdGkiOiJRcUZsVjRVUFR5U00zMk85Iiwic3ViIjowLCJwcnYiOiJmYzQ5NTU4OTUyNmI3NjRiM2VjOGE5NWZlNjkwN2U1N2VmOWI4YzRjIn0.mbp4VBPWkKez7KHsjfJiLJK7FMTCHJvgeuIMD4saOFA";
    Dio dio = Dio();
    dio.interceptors.add(
        InterceptorsWrapper(
            onRequest: (request, handler) {
              request.headers['Authorization'] = '$token';
              return handler.next(request);
            }
        )
    );

    dio.get("https://sfa.forcapos.xyz/api/news").then((response) {
      print('Data news : ${response.data}');
    }).catchError((error) {
      print('Error on : ${error.toString()}');
    });
  }
}