import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ContohDio extends StatefulWidget {
  const ContohDio({Key? key}) : super(key: key);

  @override
  _ContohDioState createState() => _ContohDioState();
}

class _ContohDioState extends State<ContohDio> {
  File? _image;
  var _imagePicker;
  String? _statusUpload;

  @override
  void initState() {
    super.initState();
    _imagePicker = ImagePicker();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Contoh Upload"),
        ),
        body: Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                    onPressed: () {
                      this._getImage();
                    },
                    child: Text("Pilih Gambar")),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: _image == null
                    ? Container(child: Text("Belum Ada Foto"))
                    : Image.file(_image!, fit: BoxFit.contain),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                    onPressed: () {
                      this._sendImage();
                    },
                    child: Text("Kirim")),
              ),
              Builder(builder: (BuildContext context) {
                if (_statusUpload == null) {
                  return Text('Belum Upload');
                } else if (_statusUpload == 'upload') {
                  return Text('Sedang Upload');
                } else if (_statusUpload == 'sukses') {
                  return Text('Upload Berhasil');
                }

                return Text('Gagal Upload');
              })
            ],
          ),
        ));
  }

  Future<void> _getImage() async {
    final XFile image = await _imagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = File(image.path);
    });
  }

  Future<void> _sendImage() async {
    setState(() {
      _statusUpload = 'upload';
    });

    final String photoName = _image!.path.split('/').last;
    Map<String, dynamic> map = {
      "image": await MultipartFile.fromFile(_image!.path, filename: photoName)
    };

    final FormData formData = FormData.fromMap(map);

    Dio dio = Dio();
    dio
        .post('https://sfa.forcapos.xyz/api/uploadImage', data: formData)
        .then((response) {
      if (response.statusCode == 200) {
        setState(() {
          _statusUpload = 'sukses';
        });
        print('Data : ${response.data}');
      } else {
        setState(() {
          _statusUpload = 'gagal';
        });
      }
    });
  }
}
