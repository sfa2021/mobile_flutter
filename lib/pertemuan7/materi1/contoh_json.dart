import 'dart:convert';

import 'package:flutter/material.dart';

class ContohJson extends StatefulWidget {
  const ContohJson({Key? key}) : super(key: key);

  @override
  _ContohJsonState createState() => _ContohJsonState();
}

class _ContohJsonState extends State<ContohJson> {
  var _jsonData = '{"nama":"Elfas","profesi":"Flutter Dev"}';
  var _parseJSON;

  @override
  void initState() {
    super.initState();
    setState(() {
      _parseJSON = json.decode(_jsonData);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contoh JSON'),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('Nama : ${_parseJSON['nama']}'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('Profesi : ${_parseJSON['profesi']}'),
            ),
          ],
        ),
      ),
    );
  }
}
