class Data {
  String? id;
  String? namaPemateri;
  List<String>? taks;

  Data({
      this.id, 
      this.namaPemateri, 
      this.taks});

  Data.fromJson(dynamic json) {
    id = json['id'];
    namaPemateri = json['nama_pemateri'];
    taks = json['taks'] != null ? json['taks'].cast<String>() : [];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = id;
    map['nama_pemateri'] = namaPemateri;
    map['taks'] = taks;
    return map;
  }
}