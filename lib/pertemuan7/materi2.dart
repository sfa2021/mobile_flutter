import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan5/materi2/custome_appbar.dart';

class P7Materi2 extends StatefulWidget {
  @override
  _P7Materi2State createState() => _P7Materi2State();
}

class _P7Materi2State extends State<P7Materi2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF3F5F7),
      appBar: CustomeAppBar(title: "Custome App Bar"),
    );
  }
}
