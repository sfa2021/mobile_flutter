import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan2/materi3/column.dart';
import 'package:mobile_flutter/pertemuan2/materi3/expanded.dart';
import 'package:mobile_flutter/pertemuan2/materi3/flexible.dart';
import 'package:mobile_flutter/pertemuan2/materi3/row.dart';
import 'package:mobile_flutter/pertemuan2/materi3/stack.dart';

class Materi3 extends StatefulWidget {
  const Materi3({Key? key}) : super(key: key);

  @override
  _Materi3State createState() => _Materi3State();
}

class _Materi3State extends State<Materi3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Materi 3"),
      ),
      body: ListView(
        children: [
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (route) => RowPage(),
                ),
              );},
            title: Text('Row'),
            subtitle: Text('Untuk membuat layout konten berjajar kesamping'),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (route) => ColumnPage(),
                ),
              );},
            title: Text('Column'),
            subtitle: Text('Untuk membuat layout konten berjajar kebawah'),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (route) => StackPage(),
                ),
              );},
            title: Text('Stack'),
            subtitle: Text('Untuk membuat layout konten bertumpuk'),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (route) => ExpandedPage(),
                ),
              );},
            title: Text('Expanded'),
            subtitle: Text('Untuk mengoptimalkan space yang kosong'),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (route) => FlexiblePage(),
                ),
              );},
            title: Text('Flexible'),
            subtitle: Text('Agar tampilan bisa menyesuaikan layar'),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          ),
        ],
      ),
    );
  }
}
