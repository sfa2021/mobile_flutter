import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ExpandedPage extends StatefulWidget {
  const ExpandedPage({Key? key}) : super(key: key);

  @override
  _ExpandedPageState createState() => _ExpandedPageState();
}

class _ExpandedPageState extends State<ExpandedPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Expanded"),
      ),
      body: Column(
        children: [
          Row(
            children: [
              Expanded(
                child:
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                    color: Colors.red,
                  ),
                  margin: EdgeInsets.all(8),
                  height: 70,
                  width: 70,
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  color: Colors.green,
                ),
                margin: EdgeInsets.all(8),
                height: 70,
                width: 70,
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  color: Colors.blue,
                ),
                margin: EdgeInsets.all(8),
                height: 70,
                width: 70,
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                flex: 3,
                child:
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                    color: Colors.red,
                  ),
                  margin: EdgeInsets.all(8),
                  height: 70,
                  width: 70,
                  child: Center(child: Text("flex = 3", style: TextStyle(fontSize: 30, color: Colors.white),)),
                ),
              ),
              Expanded(
                flex: 2,
                child:
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                    color: Colors.green,
                  ),
                  margin: EdgeInsets.all(8),
                  height: 70,
                  width: 70,
                  child: Center(child: Text("flex = 2", style: TextStyle(fontSize: 30, color: Colors.white),)),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
