import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StackPage extends StatefulWidget {
  const StackPage({Key? key}) : super(key: key);

  @override
  _StackPageState createState() => _StackPageState();
}

class _StackPageState extends State<StackPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Stack"),
      ),
      body: Stack(
        alignment: AlignmentDirectional.center,
        overflow: Overflow.visible,
        fit: StackFit.loose,
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25)),
              color: Colors.red,
            ),
            // margin: EdgeInsets.all(8),
            height: 200,
            width: double.infinity,
          ),
          Positioned(
              bottom: -40,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  color: Colors.blue,
                ),
                margin: EdgeInsets.all(8),
                height: 70,
                width: MediaQuery.of(context).size.width - 100,
              )),
          Positioned(
            bottom: -55,
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.green,
              ),
              margin: EdgeInsets.all(8),
              height: 100,
              width: 100,
            ),
          ),
        ],
      ),
    );
  }
}
