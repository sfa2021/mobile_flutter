// Copyright 2019 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter_lorem/flutter_lorem.dart';

class _TextStyleItem extends StatelessWidget {
  const _TextStyleItem({
    Key? key,
    @required this.name,
    @required this.style,
    @required this.text,
    this.textAlignment,
    this.child,
  })  : assert(name != null),
        assert(style != null),
        assert(text != null),
        super(key: key);

  final String? name;
  final TextStyle? style;
  final String? text;
  final dynamic textAlignment;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: 72,
            child: Text(name ?? 'name',
                style: Theme.of(context).textTheme.caption),
          ),
          Expanded(
            child: child ??
                Text(text ?? 'text', style: style, textAlign: textAlignment),
          ),
        ],
      ),
    );
  }
}

class TypographyDemo extends StatelessWidget {
  const TypographyDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _listText = [
      Text(
        'Flutter Academy',
        style: Theme.of(context).textTheme.bodyText2?.copyWith(
          color: Colors.red,
        ),
      ),
      Text(
        'Flutter Academy',
        style: Theme.of(context).textTheme.bodyText2?.copyWith(
          fontStyle: FontStyle.normal,
        ),
      ),
      Text(
        'Flutter Academy',
        style: Theme.of(context).textTheme.bodyText2?.copyWith(
          fontWeight: FontWeight.bold,
        ),
      ),
      Text(
        'Flutter Academy',
        style: Theme.of(context).textTheme.bodyText2?.copyWith(
          fontStyle: FontStyle.italic,
        ),
      ),
    ];

    final textTheme = Theme.of(context).textTheme;
    final styleItems = [
      // ..._listText.map((e) => SizedBox(height: 150, child: Center(child: e))).toList(),
      _TextStyleItem(
        name: 'Headline 1',
        style: textTheme.headline1?.copyWith(
            // color: Colors.pink,
            // fontWeight: FontWeight.bold,
            // fontStyle: FontStyle.italic,
            // decoration: TextDecoration.lineThrough,
            // decoration: TextDecoration.overline,
            // decoration: TextDecoration.combine([
            //   TextDecoration.lineThrough,
            //   TextDecoration.underline,
            // ]),
            ),
        text: 'Light 96sp',
      ),
      _TextStyleItem(
        name: 'Headline 2',
        style: textTheme.headline2,
        text: 'Light 60sp',
      ),
      _TextStyleItem(
        name: 'Headline 3',
        style: textTheme.headline3,
        text: 'Regular 48sp',
      ),
      _TextStyleItem(
        name: 'Headline 4',
        style: textTheme.headline4,
        text: 'Regular 34sp',
      ),
      _TextStyleItem(
        name: 'Headline 5',
        style: textTheme.headline5,
        text: 'Regular 24sp',
      ),
      _TextStyleItem(
        name: 'Headline 6',
        style: textTheme.headline6,
        text: 'Medium 20sp',
      ),
      _TextStyleItem(
        name: 'Subtitle 1',
        style: textTheme.subtitle1,
        text: 'Regular 16sp',
      ),
      _TextStyleItem(
        name: 'Subtitle 2',
        style: textTheme.subtitle2,
        text: 'Medium 14sp',
      ),
      _TextStyleItem(
        name: 'Body Text 1',
        style: textTheme.bodyText1,
        text: 'Regular 16sp',
      ),
      _TextStyleItem(
        name: 'Body Text 2',
        style: textTheme.bodyText2,
        text: 'Regular 14sp',
      ),
      _TextStyleItem(
        name: 'Button',
        style: textTheme.button,
        text: 'MEDIUM (ALL CAPS) 14sp',
      ),
      _TextStyleItem(
        name: 'Caption',
        style: textTheme.caption,
        text: 'Regular 12sp',
      ),
      _TextStyleItem(
        name: 'Overline',
        style: textTheme.overline,
        text: 'REGULAR (ALL CAPS) 10sp',
      ),
      _TextStyleItem(
        name: 'TextAlign',
        // name: '${textTheme.headline1?.fontFamily}',
        style: textTheme.bodyText2,
        text: '',
        child: Text(
          lorem(paragraphs: 1),
          // textAlign: TextAlign.center,
          // textAlign: TextAlign.right,
          textAlign: TextAlign.justify,
          // overflow: TextOverflow.ellipsis,
          overflow: TextOverflow.fade,
          maxLines: 4,
          style: TextStyle(
            fontFamily: 'TegakBersambung',
            fontSize: 24,
          ),
        ),
      ),
      RichText(
        softWrap: true,
        // maxLines: 1,
        textAlign: TextAlign.justify,
        text: TextSpan(
          children: [
            TextSpan(
              text: 'SISI',
              style: TextStyle(color: Colors.red),
            ),
            TextSpan(
              text: ' Flutter ',
              style: TextStyle(color: Colors.green),
            ),
            TextSpan(
              text: 'Academy ',
              style: TextStyle(color: Colors.blue),
            ),
            TextSpan(
              text: lorem(paragraphs: 1, words: 10),
              style: TextStyle(color: Colors.black),
            ),
          ],
        ),
      ),
      Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: RichText(
          softWrap: true,
          // maxLines: 1,
          textAlign: TextAlign.justify,
          text: TextSpan(
            children: [
              TextSpan(
                text: 'SISI',
                style: TextStyle(color: Colors.red),
              ),
              TextSpan(
                text: ' Flutter ',
                style: TextStyle(color: Colors.green),
              ),
              TextSpan(
                text: 'Academy ',
                style: TextStyle(color: Colors.blue),
              ),
              TextSpan(
                text: lorem(paragraphs: 1, words: 10),
                style: TextStyle(color: Colors.black),
              ),
            ],
          ),
        ),
      ),
      RichText(
        text: TextSpan(children: [
          TextSpan(text: '9:30 - 2:30', style: TextStyle(color: Colors.black)),
          WidgetSpan(
            child: Transform.translate(
              offset: const Offset(2, -6),
              child: Text(
                '+2',
                //superscript is usually smaller in size
                textScaleFactor: 0.7,
                style: TextStyle(color: Colors.red),
              ),
            ),
          )
        ]),
      ),
      RichText(
        text: TextSpan(children: [
          TextSpan(text: '9:30 - 2:30', style: TextStyle(color: Colors.black)),
          WidgetSpan(
            child: Transform.translate(
              offset: const Offset(2, 0),
              child: Text(
                '+2',
                //superscript is usually smaller in size
                textScaleFactor: 0.7,
                style: TextStyle(color: Colors.red),
              ),
            ),
          ),
        ]),
      )
    ];

    return Scaffold(
      appBar: AppBar(
        // automaticallyImplyLeading: false,
        title: Text('Tipografi'),
      ),
      body: Scrollbar(child: ListView(children: styleItems)),
    );
  }
}
