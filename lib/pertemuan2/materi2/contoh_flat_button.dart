import 'package:flutter/material.dart';

class ContohFlatButton extends StatefulWidget {
  const ContohFlatButton({Key? key}) : super(key: key);

  @override
  _ContohFlatButtonState createState() => _ContohFlatButtonState();
}

class _ContohFlatButtonState extends State<ContohFlatButton> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contoh Flat Button"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: FlatButton(
                child: Text("Ini Flat Button"),
                color: Colors.redAccent,
                onPressed: () {
                  print("Saya telah klik FlatButton");
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3))),
          ),
        ],
      ),
    );
  }
}
