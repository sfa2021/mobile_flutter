import 'package:flutter/material.dart';

class ContohOutlineButton extends StatefulWidget {
  const ContohOutlineButton({ Key? key }) : super(key: key);

  @override
  _ContohOutlineButtonState createState() => _ContohOutlineButtonState();
}

class _ContohOutlineButtonState extends State<ContohOutlineButton> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Contoh Outline Button"),),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Container(
              child: OutlineButton(
                child: Text("Ini OutlineButton"),
                borderSide: BorderSide(color: Colors.blue),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                onPressed: () {
                  print("Ini contoh OutlineButton");
                },
              ),
            ),
          ),
          Center(
            child: Container(
              child: OutlineButton.icon(
                  onPressed: () {
                    print("Contoh outline button with Icon");
                  },
                  icon: Icon(Icons.subscript_sharp),
                  label: Text("Outline Icon")),
            ),
          ),
        ],
      ),
    );
  }
}