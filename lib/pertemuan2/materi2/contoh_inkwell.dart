import 'package:flutter/material.dart';

class ContohInkWell extends StatefulWidget {
  const ContohInkWell({ Key? key }) : super(key: key);

  @override
  _ContohInkWellState createState() => _ContohInkWellState();
}

class _ContohInkWellState extends State<ContohInkWell> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(""),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: InkWell(
              child: Text("Ini Inkwell"),
              onTap: () {
                print("Saya telah klik InkWell");
              },
            ),
          ),
      ],),
    );
  }
}