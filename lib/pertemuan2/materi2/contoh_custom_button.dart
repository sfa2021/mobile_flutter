import 'package:flutter/material.dart';

class ContohCustomButton extends StatefulWidget {
  const ContohCustomButton({ Key? key }) : super(key: key);

  @override
  _ContohCustomButtonState createState() => _ContohCustomButtonState();
}

class _ContohCustomButtonState extends State<ContohCustomButton> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contoh Custom Button"),
      ),
      body: Container(
        child: this.customButton(),
      ),
    );
  }

  Widget customButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 1),
          child: FlatButton(
              onPressed: () {},
              child: Text("Kiri"),
              color: Colors.deepOrange,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(15),
                      topLeft: Radius.circular(15)))),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 1),
          child: FlatButton(
              onPressed: () {},
              child: Text("Kanan"),
              color: Colors.indigoAccent,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(15),
                      topRight: Radius.circular(15)))),
        )
      ],
    );
  }
}