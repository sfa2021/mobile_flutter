import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile_flutter/pertemuan2/materi2/contoh_custom_button.dart';
import 'package:mobile_flutter/pertemuan2/materi2/contoh_flat_button.dart';
import 'package:mobile_flutter/pertemuan2/materi2/contoh_icon_button.dart';
import 'package:mobile_flutter/pertemuan2/materi2/contoh_inkwell.dart';
import 'package:mobile_flutter/pertemuan2/materi2/contoh_outline_button.dart';
import 'package:mobile_flutter/pertemuan2/materi2/contoh_raised_button.dart';

// materi
// raised button, flat, icon, inkwell

class Materi2 extends StatefulWidget {
  const Materi2({Key? key}) : super(key: key);

  @override
  _Materi2State createState() => _Materi2State();
}

class _Materi2State extends State<Materi2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Contoh Button"),
        ),
        body: ListView(
          children: [
            list(
                title: "Contoh RaisedButton",
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (route) => ContohRaisedButton()),
                  );
                }),
            list(
                title: "Contoh FlatButton",
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (route) => ContohFlatButton()),
                  );
                }),
            list(
                title: "Contoh OutlineButton",
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (route) => ContohOutlineButton()),
                  );
                }),
            list(
                title: "Contoh IconButton",
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (route) => ContohIconButton()),
                  );
                }),
            list(
                title: "Contoh InkWell",
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (route) => ContohInkWell()),
                  );
                }),
            list(
                title: "Contoh CustomButton",
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (route) => ContohCustomButton()),
                  );
                }),
          ],
        ));
  }

  Widget list({String? title, GestureTapCallback? onTap}) {
    return ListTile(
      title: Text('$title'),
      trailing: Icon(Icons.arrow_right),
      onTap: onTap,
    );
  }
}
